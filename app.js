var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var login = require('./lib/passport');
var session = require('express-session')

var app = express();
var passport = require('passport')
  , util = require('util')
  , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

app.use(express.static(__dirname + '/public'));	

app.use(session({
  secret: 'keyboard cat'
}));
passport.use(login.strategy);

app.use(passport.initialize());

app.use(passport.session());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); 

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(require('./routes'));

app.listen(8080);
