angular
	.module('Report')
	.controller('PushController', PushController);

PushController.$inject = [
		'$window'
	,	'$animate'
	,	'$location'
	,	'$rootScope'
	,	'$scope'
	,	'$routeParams'
	,	'$route'
	,	'MainService'
];

function PushController($window, $animate,$location, $rootScope, $scope, $routeParams, $route, MainService){
	var report = {};
	var aplhaId = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 15);

	MainService.checkUser().then(function(res){
		var account = res.data;
		if( !account.user || account.user.error ){
		   $window.location.href = '#/login';
		   return;
		}else{
		   $scope.user = account.user;
			MainService.fetchReports().then(function(res){
				$scope.products = res.data;
				$scope.total = parseInt(($scope.products.length - 1) / 10 + 1);
			});      
		}
	});

	$scope.$watch('meta', function(){

		$scope.meta = $scope.meta.replace(/\n/g, '');

		console.log($scope.meta);
	})

	$scope.saveReport = function(){

		MainService.pushReport({
			alpha_id: aplhaId,
			target: $scope.target,            
			meta: $scope.meta,
			time: new Date().toISOString().slice(0, 19).replace('T', ' '),
			product: $scope.product
		}).then(function(res){

		});
	}

};