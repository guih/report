angular
	.module('Report')
	.controller('MainController', MainController);

MainController.$inject = [ 
		'$window'
	,	'$animate'
	,	'$location'
	,	 '$rootScope'
	,	 '$scope'
	,	 '$routeParams'
	,	 '$route'
	,	 'MainService'
];

function MainController( $window,$animate,$location, $rootScope, $scope, $routeParams, $route, MainService){

	var vm = this;
	$animate.enabled(false);
	$rootScope.$on("$locationChangeStart", function (event, next, current) {
		$animate.enabled(true);
	});
	$scope.currentPage = 0;
	$scope.pageSize = 10;
	$scope.data = [];

	MainService.checkUser().then(function(res){
		var account = res.data;
		if( !account.user || account.user.error ){
			
		   $window.location.href = '#/login';
		   return;
		}else{

		   $window.location.href = '#/';

		   $scope.user = account.user;
			MainService.fetchReports().then(function(res){
				$scope.reports = res.data;
				$scope.total = parseInt(($scope.reports.length - 1) / 10 + 1);
			});          
		}
	});
}