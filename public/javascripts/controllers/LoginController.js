angular
    .module('Report')
    .controller('LoginController', LoginController);

LoginController.$inject = ['$window','$animate','$location', '$rootScope', '$scope', '$routeParams', '$route', 'MainService'];

function LoginController($window, $animate,$location, $rootScope, $scope, $routeParams, $route, MainService){
    $scope.title = "Sign"
    MainService.checkUser().then(function(res){
        var account = res.data;
        if( account.error || account.user.error ){
        	if( account.user ){
           		$scope.error = account.user.error;
        	}else{
        		$scope.error = "";
        	}
        }else{
           $window.location.href = '#/';  
        }
    });
};