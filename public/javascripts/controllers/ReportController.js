angular
	.module('Report')
	.controller('ReportController', ReportController);

ReportController.$inject = [
		'$window'
	,	'MainService'
	,	'$rootScope'
	,	'$scope'
	,	'$routeParams'
	,	'$route'
	,	'$locale'
];

function ReportController( $window, MainService, $rootScope, $scope, $routeParams, $route, $locale) {
	var vm = this;   
	var product = null;        


	$scope.currentLanguage = "en";

	MainService.checkUser().then(function(res){
		var account = res.data;
		if( !account.user || account.user.error ){
		   $window.location.href = '#/login';
		   return;
		}else{
		   $scope.user = account.user; 
		}
	});

	$rootScope.$watch('$routeChangeSuccess', function () {
		$scope.id = $routeParams.id;
		MainService.fetchSingle( $scope.id ).then(function(report){			

			$scope.data = report.data;
			$scope.data.meta = JSON.parse( report.data.meta );

			switch( report.data.product ){
				case "2":
					product = "One Day";
				break;				
				case "3":
					product = "Care";
				break;				
				case "4":
					product = "Customize"
				break;
			}
			
			$scope.data.product = product;
			$scope.report = $scope.data;

		});
	});

	
	$scope.switchLang = function (lang) {
		$scope.currentLanguage = lang;
	}    

}