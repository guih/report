angular
    .module('Report')
    	.factory('MainService', MainService);
MainService.$inject = ['$http'];

function MainService($http){
	var reports = {};

	reports.pushReport = function(meta){
		return $http.post('/reports', { 
			type :'push',
			meta: meta
		});
	};

	reports.fetchSingle = function(_id){
		return $http.post('/reports', { 
			type :'single',
			id: _id
		});
	}
	
	reports.fetchReports = function(){
		return $http.post('/reports', { 
			type :'fetch' 
		});
	};
	reports.checkUser = function(){
		return $http.post('/user', {
			_: +new Date()
		});
	}

	return reports;
}