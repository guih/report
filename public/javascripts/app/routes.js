angular
	.module('Report', ['ngRoute', 'ngAnimate'])
	.config(['$routeProvider', '$locationProvider',
	function ($routeProvider, $locationProvider) {
		$routeProvider.
		when('/', {
			templateUrl: 'templates/main.html',
			controller: 'MainController',
			controllerAs: 'Main'
		}).
		when('/login', {
			templateUrl: 'templates/login.html',
			controller: 'LoginController',
			controllerAs: 'Login'
		}).
		when('/push', {
			templateUrl: 'templates/push.html',
			controller: 'PushController',
			controllerAs: 'Push'
		}).
		when('/report/:id', {
			templateUrl: 'templates/report.html',
			controller: 'ReportController',
			controllerAs: 'Report'
		}).otherwise({
			redirectTo: '/'
		});
	}]);