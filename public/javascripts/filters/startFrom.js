angular
	.module('Report')
	.filter('startFrom', startFrom);

function startFrom(){
	return function(input, start) {
		start = +start;
		if( input )
			return input.slice(start);
	}
}