angular
    .module('Report')
    .filter('i18n', i18n);
    
i18n.$inject = ['$rootScope'];


function i18n($rootScope) {
    return function (input) {

        var currentLanguage = $rootScope.currentLanguage || 'en';

        return translations[currentLanguage][input];
    }
}