var express                 = require("express");
var app                     = express();
var passport                = require('passport');
var GoogleStrategy          = require('passport-google-oauth').OAuth2Strategy;
var db                      = require('../lib/db');
var staticVars              = require('../lib/static');



passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

exports.auth = passport.authenticate('google', { 
    scope: staticVars.CLIENT_SCOPE
});

exports.cb = passport.authenticate('google', { 
    failureRedirect: '#/login' 
});

exports.strategy = new GoogleStrategy({
        clientID        : staticVars.CLIENT_ID,
        clientSecret    : staticVars.CLIENT_SECRET,
        callbackURL     : staticVars.CALLBACK_URL
    },
    function(req, accessToken, refreshToken, profile, done) {
        process.nextTick(function () {
            var user = profile;

            var userMeta = user._json;
            var desiredUser = {
                name: userMeta.name.givenName,
                picture:  userMeta.image.url,
                emailDomain: userMeta.domain,  
                level: 0,
                error: null
            };
            if( userMeta.domain != "apiki.com" ){
                return done( null, {
                    error: "Only people with @Apiki.com account can signin."
                });
            }

            db.query('SELECT * from users where 1', function(err, rows, fields){
                 if (err) throw err;

                 if( rows.length > 0 ){
                    for( var i = 0; i< rows.length; i++ ){
                        var uMeta = JSON.parse(rows[i].meta);
                        if( uMeta.name == userMeta.name.givenName ){
                            req.sessionId = uMeta.name + uMeta.domain;
                            return done(null, uMeta);
                        }
                    }                  
                }

                db.query('INSERT INTO users set ?', {
                    meta: JSON.stringify(desiredUser)
                },function(err, rows, fields) {
                    if (err) throw err;

                    req.sessionId = userMeta.name.givenName + userMeta.domain;
                    return done(null, desiredUser);
                });
                
            })
  

            
        });
    }
);
exports.ensureAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/login');
}
