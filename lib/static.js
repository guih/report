module.exports = {
	CLIENT_ID		: "113309572302-qnc7o6kfo3ssjellqd9i5k664vmnn07d.apps.googleusercontent.com",
	CLIENT_SECRET	: "W4J5UtJb6hiOaSzsFJFGBu5z",
	CALLBACK_URL	: "http://localhost:8080/auth/google/callback",
	CLIENT_SCOPE	: [
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile'
    ],
    REPORT_STATIC : {
    	pt: {
    		title: "Apiki WP Análise de segurança",
    		reporter: "responsável",
    		generated: "hora",
    		advise: "Segurança de aplicações web é uma preocupação cada vez maior. Devido ao design, aplicações Web recebem entradas(stdin) cuja fonte não é confiável,	foram realizadas numerosas operações sensíveis à segurança (como o banco de dados, acesso à arquivos e transferências de conteúdo da Web para máquinas remotas) na tentaiva de expor dados aos observadores potencialmente maliciosos .",
    		introduction: "Introdução",
    		target: "Alvo",
    		affected: "Produto afetado",
    		fixed: "Atualizado",
    		type: "Tipo de vulnerabilidade",
    		remoteExploitable: "Remotamente explorável",
    		vendor: "Notificado ao desenvolvedor" ,
    		toPublic: "Vazado ao público",
    		credits: "Créditos",
    		level: "Nível de ameaça",
    		solution: "Solução",
    		reference: "Referência"
    	},
    	en:{
    		title: "Apiki WP Security Analysis",
    		reporter: "reporter",
    		generated: "generated",
    		advise: "Web application security is an ever-growing concern. By design, Web applications feed on inputs whose source is untrusted, we performed numerous security-sensitive operations (such as database accesses and transfers of Web content to remote machines), and expose data to potentially malicious observers.",
    		introduction: "Introduction",
    		target: "Target",
    		affected: "Affected product",
    		fixed: "Fixed in",
    		type: "Vulnerability Type",
    		remoteExploitable: "Remote Exploitable",
    		vendor: "Reported to vendor" ,
    		toPublic: "Disclosed to public",
    		credits: "Credits",
    		level: "Threat level",
    		solution: "Solution",
    		reference: "Reference"
    	}
    }
};