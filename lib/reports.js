var db = require('./db');
var staticVars = require('./static');


exports.getReports = function(fn){
	var records  = {};

	db.query('SELECT * from reports where 1 ORDER BY id DESC', function(err, rows, fields){
		if (err) {
			throw err;
		}

		if( rows.length > 0 ){
			fn( records = rows );
		}else{
			fn({});
		}
	});	
}
exports.getReport = function(alphaId, fn){
	var record  = {};

	db.query('SELECT * from reports where alpha_id = ?', [alphaId], function(err, rows, fields){
		if (err) {
			throw err;
		}
		record = rows[0];
		record.vars = staticVars.REPORT_STATIC;

		if( rows.length > 0 ){
			fn( record );
		}else{
			fn({});
		}
	});		
}
exports.insertReport = function(meta, fn){
	db.query('INSERT INTO reports set ?', meta,function(err, rows, fields) {
		if (err) {
			fn({
				error: true,
				caption: "Inserted failed!"
			});
			throw err;
		}

		fn({
			error: false,
			caption: "Inserted succeful!"
		});
	});	
}
exports.checkReport = function(){
	
}