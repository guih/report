var express = require('express');
var app = module.exports = express();
var login = require('../lib/passport');
var db = require('../lib/db');
var reports = require('../lib/reports');




app.get('/auth/google',login.auth, function(req, res){

});

app.get('/auth/google/callback', login.cb, function(req, res) {
    res.redirect('/');
});

app.get('/logout', function(req, res){
	req.logout();
	res.redirect('/');
});

app.get('/', function(req, res){
	// if( !req.user ){
	// 	res.redirect('/login');
	// }

	res.render('index');
});

app.get('/account', login.ensureAuthenticated, function(req, res){
	res.render('account', { user: req.user });
});

app.post('/user', function(req, res){
	if( req.user ){
		res.json({ loggedIn: false, error: false, user: req.user,  redirect: "#/dashboard"});	
	}else{
		res.json({ loggedIn: false, error: true, user: req.user,  redirect: "#/login"});	

	}
});


app.post('/reports', function(req, res, next) {

	if( !req.user ){
		res.json({error: true, caption: "You're not loggedin"});
		return;
	}

	var post = req.body;
	var meta = {};
	var holder = "";

	if( post.type == "push" ){

		meta = post.meta;
		reports.insertReport(meta, function(result){
			res.json(result);
		});
		return;
	}
	if( post.type == "fetch" ){
		reports.getReports(function(result){
			res.json(result);
		
		});
		return;
	}
	if( post.type == "single" ){
		reports.getReport(post.id, function(result){
			res.json(result);
		});
		return;
	}
});